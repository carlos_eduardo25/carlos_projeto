<?
require '../uteis.php';

$adm = new Admin();
if($adm->setAdmin($_POST)){
    $result = array(
        'status' => 'success',
        'msg' => 'Seu registro foi cadastrado',
    );

    echo json_encode($result);
}
else{
    $result = array(
        'status' => 'danger',
        'msg' => 'Parabéns, seu registro não pode ser cadastrado',
    );

    echo json_encode($result); 
}
?>