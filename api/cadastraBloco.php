<?
require '../uteis.php';

$bloco = new Bloco();
if($bloco->setBloco($_POST)){
    $result = array(
        'status' => 'success',
        'msg' => 'Seu registro foi cadastrado',
    );

    echo json_encode($result);
}
else{
    $result = array(
        'status' => 'danger',
        'msg' => 'Parabéns, seu registro não pode ser cadastrado',
    );

    echo json_encode($result); 
}
?>