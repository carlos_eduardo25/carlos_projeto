<?
require '../uteis.php';

$sindico = new Sindico();
if($sindico->setSindico($_POST)){
    $result = array(
        'status' => 'success',
        'msg' => 'Seu registro foi cadastrado',
    );

    echo json_encode($result);
}
else{
    $result = array(
        'status' => 'danger',
        'msg' => 'Parabéns, seu registro não pode ser cadastrado',
    );

    echo json_encode($result); 
}
?>