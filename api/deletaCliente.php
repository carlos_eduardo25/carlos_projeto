<?
require '../uteis.php';

$cliente = new Cadastro();
$result = $cliente->deletaCliente($_POST['id']);
if($result){

    $totalRegistros = $cliente->getClientes()['totalResults'];
    
    $result = array(
        'status' => 'success',
        'totalRegistros' => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        'msg' => 'Parabéns, seu registro foi deletado',
    );

    echo json_encode($result);
}
else{
    $result = array(
        'status' => 'danger',
        'msg' => 'Parabéns, seu registro não pode ser deletado',
    );

    echo json_encode($result);
}

?>