<?
require '../uteis.php';

$adm = new Admin();
if($adm->editAdmin($_POST)){
    $result = array(
        'status' => 'success',
        'msg' => 'Seu registro foi editado',
    );

    echo json_encode($result);
}
else{
    $result = array(
        'status' => 'danger',
        'msg' => 'Parabéns, seu registro não pode ser editado',
    );

    echo json_encode($result);
}
?>