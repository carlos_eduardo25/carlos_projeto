<?
require '../uteis.php';

$sindico = new Sindico();
if($sindico->editSindico($_POST)){
    $result = array(
        'status' => 'success',
        'msg' => 'Seu registro foi editado',
    );

    echo json_encode($result);
}
else{
    $result = array(
        'status' => 'danger',
        'msg' => 'Parabéns, seu registro não pode ser editado',
    );

    echo json_encode($result);
}
?>