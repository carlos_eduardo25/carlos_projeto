<?
$condominio = new Condominio();
$listCondominio = $condominio->getCondominio();

$clientes = new Cadastro();
$clientes->pagination = 6;

if(isset($_GET['b'])){
    $moraBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $moraBusca['nome'] = $termo;
                break;
            case 'termo2':
                $moraBusca['from_condominio'] = $termo;
                break;
            default:
                # code...
                break;
        }
        
    }
}

$clientes->busca = $moraBusca;
$result = $clientes->getClientes();

$paginacao = ($result['totalResults'] > $clientes->pagination) ? $clientes->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResults']) < 10 ? '0'.$result['totalResults'] : $result['totalResults'];
?>