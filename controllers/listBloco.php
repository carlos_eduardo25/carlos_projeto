<?
$bloco = new Bloco();
$bloco->pagination = 6;

if(isset($_GET['b'])){
    $blocoBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $blocoBusca['nomeBloco'] = $termo;
                break;
            default:
                # code...
                break;
        }
        
    }
}

$bloco->busca = $blocoBusca;
$result = $bloco->getBloco();

$paginacao = ($result['totalResults'] > $bloco->pagination) ? $bloco->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];
?>