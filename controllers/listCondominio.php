<?
$condominios = new Condominio();
$condominios->pagination = 6;

if(isset($_GET['b'])){
    $condoBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $condoBusca['nomeCondo'] = $termo;
                break;
            default:
                # code...
                break;
        }
        
    }
}

$condominios->busca = $condoBusca;
$result = $condominios->getCondominio();

$paginacao = ($result['totalResults'] > $condominios->pagination) ? $condominios->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];
?>