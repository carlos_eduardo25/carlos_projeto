<?
$sindicos = new Sindico();
$sindicos->pagination = 6;

if(isset($_GET['b'])){
    $sindBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $sindBusca['sindico'] = $termo;
                break;
            default:
                # code...
                break;
        }
        
    }
}

$sindicos->busca = $sindBusca;
$result = $sindicos->getSindico();

$paginacao = ($result['totalResults'] > $sindicos->pagination) ? $sindicos->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResults']) < 10 ? '0'.$result['totalResults'] : $result['totalResults'];
?>