<?
$unidades = new Unidade();
$unidades->pagination = 6;

if(isset($_GET['b'])){
    $uniBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $uniBusca['numUnidade'] = $termo;
                break;
            default:
                # code...
                break;
        }
        
    }
}

$unidades->busca = $uniBusca;
$result = $unidades->getUnidade();

$paginacao = ($result['totalResults'] > $unidades->pagination) ? $unidades->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResults']) < 10 ? '0'.$result['totalResults'] : $result['totalResults'];
?>