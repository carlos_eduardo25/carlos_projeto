<?
$users = new User();
$users->pagination = 6;

if(isset($_GET['b'])){
    $userBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $userBusca['nome'] = $termo;
                break;
            default:
                # code...
                break;
        }
        
    }
}

$users->busca = $userBusca;
$result = $users->getUser();

$paginacao = ($result['totalResults'] > $users->pagination) ? $users->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];
?>