<?
$nav = array(
    'Listar' => array(
        'listAdmin' => 'Administradoras',
        'listCondominio' => 'Condomínios',
        'listSindico' => 'Conselhos',
        'listBloco' => 'Blocos',
        'listUnidade' => 'Unidades',
        'clientes' => 'Moradores'
    ),
    'Cadastrar' => array(
        'admin' => 'Administradora',
        'condominio' => 'Condominio',
        'sindico' => 'Conselho',
        'bloco' => 'Bloco',
        'unidade' => 'Unidade',
        'cadastro' => 'Morador'
    ),
    'Usuários' => array(
        'user' => 'Cadastrar',
        'listUser' => 'Listar'
    ),
);
?>