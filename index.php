<?
include "uteis.php";
$user = new Usuario();
if(!$user->acesso()){
  header('Location: login.php');
}
if($_GET['page'] == 'logout'){
  if($user->logout()){
    header('Location: '.$url_site.'login.php');
  }
}

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>icofont/icofont.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/main.css">
    <title>Projeto</title>
</head>
<body style="background-color: rgb(187, 187, 187);">
<nav class="navbar navbar-expand-lg navbar-light bg-info">
  <a href="index.php?page=inicio" class="navbar-brand"><i class="icofont-building-alt icofont-3x logo" style="padding-right: 20px; color:black;"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="navbar-brand mr-5" href="<?=$url_site?>inicio"><b>Início</b></a>
      </li>
      <?
          foreach($nav as $ch=>$menu){
    
              if(is_array($menu)){?>
                  <li class="nav-item dropdown" style="list-style: none;">
                      <a class="navbar-brand mr-5 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                          <b><?=$ch?></b>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <? foreach($menu as $ch2=>$smenu){?>
                          <a class="dropdown-item" href="<?=$url_site.$ch2?>"><b><?=$smenu?></b></a>
                          <? } ?>
                      </div>
                  </li>
    
         <?} else{?>
          <li class="nav-item"><a class="nav-link text-light" href="<?=$url_site.$ch?>"><?=$menu?></a></li>
          <? } ?>
      <? } ?>
  </div>
  <div class="right">
    <ul class="right logpass">
      <li style="list-style: none;" class="right logpass2"><a href="<?=$url_site?>logout"><i class="icofont-logout" style="color: black;"></i></a></li>
      <li style="list-style: none;" class="right logpass"><a href="?page=configuracoes"><i class="icofont-gear-alt" style="color: black;"></i></a></li>
    </ul>
    <?
    $nome = explode(' ',$_SESSION['USUARIO']['nome'])
    ?>
    <small class="text-dark right"><b>Olá <?=$nome[0];?>, seja bem-vindo(a).</b></small>
  </div>
  </nav>
    <main class="container">
        <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "controllers/inicio.php";
                require "views/inicio.php";
                break;
            default:
                require 'controllers/'.$_GET['page'].'.php';
                require 'views/'.$_GET['page'].'.php';
                break;
        }
        ?>    
    </main>

    <footer>
        <div class="info">
            <span>&copy; Todos os direitos reservados.</span> 
        </div>

        <div class="support">
            <a href="#">(47) 90123-4567</a> 
        </div>

        <div class="version float-right">
            <small class="versionControl">v 1.0</small>
        </div>
    </footer>

    <script>var url_site = '<?=$url_site?>'</script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.bundle.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/app.js"></script>
    <script src="<?=$url_site?>js/condominio.js"></script>
    <script src="<?=$url_site?>js/bloco.js"></script>
    <script src="<?=$url_site?>js/unidade.js"></script>
    <script src="<?=$url_site?>js/sindico.js"></script>
    <script src="<?=$url_site?>js/admin.js"></script>
    <script src="<?=$url_site?>js/user.js"></script>
</body>
</html>