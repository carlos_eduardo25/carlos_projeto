$(function(){

    $('li.dropdown').hover(function(){
        console.log('teste')
        $(this).find('.dropdown-menu').stop(true,true).toggle('show');
    }, function(){
        $(this).find('.dropdown-menu').stop(true,true).toggle('hide');
    })

    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : '';

        window.location.href = url_site+pagina+'/busca/'+termo1+termo2;
        return false;
    })

    $('.termo1, .termo2').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled', false);
        }
        else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })

    $('li.Cadastrar').hover(function(){
        $('.dropdown-menu').stop(true, true).toggle('show');
    },
        function(){
            $('.dropdown-menu').stop(true, true).toggle('hide');
    })

    $('li.Usuários').hover(function(){
        $('.dropdown-menu').stop(true, true).toggle('show');
    },
        function(){
            $('.dropdown-menu').stop(true, true).toggle('hide');
    })

    $('.formMorador').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;


        if(editar){
            url = url_site+'api/editaCliente.php';
            urlRedir = url_site+'?page=clientes'
        }
        else {
            url = url_site+'api/cadastraCliente.php'
            urlRedir = url_site+'?page=cadastro'
        }

        $('.buttonEnviar').attr('disabled','disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                if(data.status == 'success'){
                    // $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    });

    $('#listaClientes').on('click', '.removerCliente', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCliente.php',
            dataType: 'json',
            type: 'POST',
            data:  {id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'clientes');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione...</option>';

        for (let i = 0; i < dados.length; i++){
    
            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
        }
        $(seletor).html(estrutura)
    }

    $('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data.resultSet, 'nomeBloco');
            }
        })
    })

    $('.fromBloco').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                selectPopulation('.fromUnidade',data.resultSet, 'numUnidade');
            }
        })
    })

});

function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert"><center>'+mensagem+'</center></div>';

    $(pai).html(componente);

    setTimeout(function(){
        $(pai).find('div.alert').remove();
        if(tipo == 'success' && url){
            setTimeout(function(){
                window.location.href = url;
            }, 500)
        }
        else{
            setTimeout(function(){
                window.location.href = url;
            }, 500)
        }
    },2000)
};

function myAlertLogin(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
      componente ='<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';
      
      $(pai).prepend(componente);

      setTimeout(function(){
          $(pai).find('div.alert').remove();
          //redirecionar?
          if(tipo == 'success' && url){ 
              setTimeout(function(){
                  window.location.href = url;
              },500)
          }
      }, 2000)
  }