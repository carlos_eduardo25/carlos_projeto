$(function(){

    $('li.dropdown').hover(function(){
        console.log('teste')
        $(this).find('.dropdown-menu').stop(true,true).toggle('show');
    }, function(){
        $(this).find('.dropdown-menu').stop(true,true).toggle('hide');
    })

    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();

        termo1 = (termo1) ? termo1+'/' : '';

        window.location.href = url_site+pagina+'/busca/'+termo1;
        return false;
    })

    $('.termo1').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        if(termo1){
            $('button[type="submit"]').prop('disabled', false);
        }
        else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })

    $('.formSindico').submit(function(){
        var sindico = $(this).find('input[name="c[sindico]"]').val();
        var subSindico = $(this).find('input[name="c[subSindico]"]').val();
        var conselheiro = $(this).find('input[name="c[conselheiro]"]').val();
        var condoSindico = $(this).find('select[name="c[condoSindico]"]').val();
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaSindico.php';
            urlRedir = url_site+'?page=listSindico';
        }
        else {
            url = url_site+'api/cadastraSindico.php';
            urlRedir = url_site+'?page=sindico';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            // data: $(form).serialize(),
            data: {
                sindico: sindico,
                subSindico: subSindico,
                conselheiro: conselheiro,
                condoSindico: condoSindico,
                editar : editar
            },
            success : function(data){
                if(data.status == 'success'){
                    // $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    })

    function myAlert(tipo, mensagem, pai, url){
        url = (url == undefined) ? url = '' : url = url;
        componente = '<div class="alert alert-'+tipo+'" role="alert"><center>'+mensagem+'</center></div>';

        $(pai).html(componente);

        setTimeout(function(){
            $(pai).find('div.alert').remove();
            if(tipo == 'success' && url){
                setTimeout(function(){
                    window.location.href = url;
                }, 500)
            }
            else{
                setTimeout(function(){
                    window.location.href = url;
                }, 500)
            }
        },2000)
    }

    $('#listaSindicos').on('click', '.removerSindico', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaSindico.php',
            dataType: 'json',
            type: 'POST',
            data:  {id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'listSindico');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
})