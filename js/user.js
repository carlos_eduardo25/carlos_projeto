$(function(){

    $('li.dropdown').hover(function(){
        console.log('teste')
        $(this).find('.dropdown-menu').stop(true,true).toggle('show');
    }, function(){
        $(this).find('.dropdown-menu').stop(true,true).toggle('hide');
    })

    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();

        termo1 = (termo1) ? termo1+'/' : '';

        window.location.href = url_site+pagina+'/busca/'+termo1;
        return false;
    })

    $('.termo1').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        if(termo1){
            $('button[type="submit"]').prop('disabled', false);
        }
        else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })

    $('.formUser').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaUser.php';
            urlRedir = url_site+'?page=listUser'
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                if(data.status == 'success'){
                    // $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    })

    function myAlert(tipo, mensagem, pai, url){
        url = (url == undefined) ? url = '' : url = url;
        componente = '<div class="alert alert-'+tipo+'" role="alert"><center>'+mensagem+'</center></div>';

        $(pai).html(componente);

        setTimeout(function(){
            $(pai).find('div.alert').remove();
            if(tipo == 'success' && url){
                setTimeout(function(){
                    window.location.href = url;
                }, 500)
            }
            else{
                setTimeout(function(){
                    window.location.href = url;
                }, 500)
            }
        },2000)
    }

    $('#listaUsers').on('click', '.removerUser', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaUser.php',
            dataType: 'json',
            type: 'POST',
            data:  {id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'listUser');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.formUser').submit(function(){
        var senha = $(this).find('#senha').val();
        var confirmaSenha = $(this).find('#csenha').val();

        if(senha == confirmaSenha){
            $.ajax({
                url: url_site+'api/cadastraUser.php',
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data){
                    if(data.status == 'success'){
                        myAlert(data.status, data.msg, 'main');
                    }
                    else{
                        myAlert(data.status, data.msg, 'main');
                    }
                }
            })
        }
        else{
            document.myAlert('danger','A senha não bate','main');
        }
        return false;
    })
})