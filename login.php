<?
include "uteis.php";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="icofont/icofont.min.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Projeto | Login</title>
</head>
<body class="bg-info login">
    
    <main class="container">
        <div class="row">
            <div class="col-4">
                <center>
                    <form class="form-signin" method="POST" action="<?=$url_site?>/controllers/restrito.php">
                        <i class="icofont-building-alt icofont-10x logo" style="padding-right: 20px; color:black;"></i>
                        <h1 class="h3 mb-3 font-weight-normal">Efetue seu login</h1>
                        <label for="usuario" class="sr-only">Usuário</label>
                        <input type="text" id="usuario" class="form-control mb-1" placeholder="Usuário" name="usuario" required autofocus>
                        <label for="inputPassword" class="sr-only">Senha</label>
                        <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Senha" required>
                        <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Entrar</button>
                    </form>
                </center>
            </div>
        </div>
        
    </main>

    <footer>
        
    </footer>


    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
    <? if(isset($_GET['msg'])){?>
    <script type="text/javascript">
        $(function(){
            myAlertLogin('danger', '<?=$_GET['msg']?>', 'main');
        })
    </script>
    <?}?>
</body>
</html>