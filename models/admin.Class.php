<?

Class Admin extends Dao{


    function __construct(){
        
    }

    function getAdmin($id = null){
        $qry = 'SELECT * FROM administradoras';
        $contaTermos = count($this->busca);

        
        if($contaTermos > 0){

            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.' '.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $unique = true;
            $qry .= ' WHERE id ='.$id;
        }
        return $this->listarData($qry, $unique);
    }

    function setAdmin($dados){
        $values = '';
        $sql = 'INSERT INTO administradoras (';
        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."',";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($sql);;
    }

    function editAdmin($dados){       
        $sql = 'UPDATE administradoras SET';
        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaAdmin($id){
        
        return $this->deletar('DELETE FROM administradoras WHERE id = '.$id);
    }

    function ultimas(){
        $sql = 'SELECT nome_adm
        FROM administradoras
        ORDER BY dataCadastro DESC
        LIMIT 5';

        return $this->listarData($sql, false);
    }
}
?>