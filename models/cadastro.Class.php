<?
Class Cadastro extends Unidade{

    function __construct(){
        
    }

    function getClientes($id = null){
        $qry = 'SELECT * FROM moradores';
        $contaTermos =count($this->busca);

        $isNull = false;

        if($contaTermos > 0){

            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .= ' WHERE id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function setCliente($dados){
        $values = '';
        $sql = 'INSERT INTO moradores (';
        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."',";
        }
        $sql = rtrim($sql,',');
        $sql .=') VALUES ('.rtrim($values,',').')';
        return $this->insertData($sql);
        
    }

    function editCliente($dados){
        
        $sql = 'UPDATE moradores SET';
        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."',";
            }
        }
        $sql = rtrim($sql,',');
        $sql .= ' WHERE id ='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaCliente($id){
        
        return $this->deletar('DELETE FROM moradores WHERE id = '.$id);
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeBloco FROM bloco WHERE condoBloco = '.$cond;
        return $this->listarData($qry, false);
    }

    function moradorCondo(){
        $qry = 'SELECT
        condo.nomeCondo,
        COUNT(morador.id) AS totalMoradores
        FROM moradores morador
        LEFT JOIN condominio condo ON condo.id = morador.from_condominio
        GROUP BY from_condominio
        ';
        
        return $this->listarData($qry, false);
    }
}

?>