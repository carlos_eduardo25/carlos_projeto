<?

Class Sindico extends Condominio{


    function __construct(){
        
    }

    function getSindico($id = null){
        $qry = 'SELECT * FROM conselho';
        $contaTermos = count($this->busca);

        
        if($contaTermos > 0){

            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .= ' WHERE id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function setSindico($dados){
        $values = '';
        $sql = 'INSERT INTO conselho (';
        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."',";
        }
        $sql = rtrim($sql,',');
        $sql .=') VALUES ('.rtrim($values,',').')';
        return $this->insertData($sql);;
    }

    function editSindico($dados){       
        $sql = 'UPDATE conselho SET';
        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."',";
            }
        }
        $sql = rtrim($sql,',');
        $sql .= ' WHERE id = '.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaSindico($id){
        
        return $this->deletar('DELETE FROM conselho WHERE id = '.$id);
    }
}
?>