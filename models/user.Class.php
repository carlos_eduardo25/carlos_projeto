<?

Class User extends Dao{

    function __construct(){
        
    }

    function getUser($id = null){
        $qry = 'SELECT * FROM usuarios';
        $contaTermos = count($this->busca);

        
        if($contaTermos > 0){

            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        else if($id){
            $qry .= ' WHERE id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }
    
    function userExists($user){
        $qry = "SELECT usuario FROM usuarios WHERE usuario = '".$user."'";
        return $this->listarData($qry,true);
    }

    function setUser($dados){
        $values = '';
        $sql = 'INSERT INTO usuarios (';
        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."',";
        }
        $sql = rtrim($sql,',');
        $sql .=') VALUES ('.rtrim($values,',').')';
        return $this->insertData($sql);
    }

    function editUser($dados){
        $sql = 'UPDATE usuarios SET';
        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."',";
            }
        }
        $sql = rtrim($sql,',');
        $sql .= ' WHERE id ='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaUser($id){
        return $this->deletar('DELETE FROM usuarios WHERE id = '.$id);
    }
}

?>