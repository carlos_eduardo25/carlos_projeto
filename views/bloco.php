<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Bloco</h1>
</center>
    <form action="" method="POST" class="formBloco">
        <div class="row">
            <div class="col-12 col-md-3">
                <select class="form-control mb-2" name="condoBloco">
                    <option value="N/A">Selecione o condomínio</option>
                    <?foreach($chamaCondo as $condo){?>
                        <option value="<?=$condo['id']?>" <?=($condo['id'] == $popular['condoBloco'] ? 'selected' : '')?>><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="nomeBloco" required placeholder="Nome do Bloco" value="<?=$popular['nomeBloco']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="andares" required placeholder="Quantidade de Andares" value="<?=$popular['andares']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control" type="text" name="unidades" required placeholder="Unidades por Andar" value="<?=$popular['unidades']?>">
            </div>
            <?if($_GET['id']){?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <?}?>
            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>