<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Morador</h1>
</center>
    <form action="" method="POST" class="formMorador">
        <div class="row">
            <div class="col-12 col-md-4">
                <select class="form-control mb-2 fromCondominio" name="from_condominio">
                    <option>Selecione o condomínio</option>
                    <?foreach($chamaCondo['resultSet'] as $condo){?>
                        <option value="<?=$condo['id']?>" <?=($condo['id'] == $popular['from_condominio'] ? 'selected' : '')?>><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4">
                <select class="form-control mb-2 fromBloco" name="from_bloco">
                    <?
                    if($_GET['id']){
                        $blocos = $clientes->getBlocoFromCond($popular['from_condominio']);
                        foreach($blocos['resultSet'] as $bloco){
                    ?>
                            <option value="<?=$bloco['id']?>" <?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                        <?}?>
                    <?}?>

                </select>
            </div>
            <div class="col-12 col-md-4">
                <select class="form-control mb-2 fromUnidade" name="from_unidade">
                    <?
                    if($_GET['id']){
                        $unidades = $clientes->getUnidadeFromCond($popular['from_bloco']);
                        foreach($unidades['resultSet'] as $unidade){
                    ?>
                            <option value="<?=$unidade['id']?>" <?=($unidade['id'] == $popular['from_unidade'] ? 'selected' : '')?>><?=$unidade['numUnidade']?></option>
                        <?}?>
                    <?}?>

                </select>
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="nome" required placeholder="Nome" value="<?=$popular['nome']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="cpf" required placeholder="CPF" value="<?=$popular['cpf']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="email" name="email" required placeholder="E-mail" value="<?=$popular['email']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control" type="tel" name="telefone" placeholder="Telefone" value="<?=$popular['telefone']?>">
            </div>
            <?if($_GET['id']){?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <?}?>
            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>