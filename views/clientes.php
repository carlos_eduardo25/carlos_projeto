<center class="mt-4 pb-4">
    <h1 class="col-12">Lista de Moradores</h1>
</center>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table mt-3" id="listaClientes">
                <thead class="thead-dark">
                    <tr>
                        <td colspan="10">
                            <form class="form-inline my-2 my-lg-0" action="<?=$url_site?>index.php" method="GET" id="filtro">
                                <input type="hidden" name="page" value="clientes">
                                <div class="input-group-prepend mb-2">
                                    <div class="input-group-text">Busca por Nome</div>
                                </div>
                                <input class="form-control mr-sm-2 mb-2 termo1" type="search" placeholder="Buscar por nome..." aria-label="Search" name="b[nome]">
                                <div class="input-group-prepend">
                                    <div class="input-group-text mb-2">Por condomínio</div>
                                </div>
                                <select class="form-control custom-select mb-2 mr-2 termo2" name="b[from_condominio]">
                                    <option value="">...</option>
                                <?
                                foreach($listCondominio['resultSet'] as $condominios){
                                    echo '<option value="'.$condominios['id'].'">'.$condominios['nomeCondo'].'</option>';
                                }
                                ?>
                                </select>
                                <button class="btn btn-outline-success mb-2 mr-2" type="submit" disabled>Buscar</button>
                                <a class="btn btn-outline-danger mb-2" href="<?=$url_site?>clientes">Limpar</a>
                            </form>
                        </td>
                    </tr>
                    <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Bloco</th>
                    <th scope="col">Unidade</th>
                    <th scope="col">Nome</th>
                    <th scope="col">CPF</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">DT. Cadastro</th>
                    <th align="center"><a href="index.php?page=cadastro" class="btn btn-info btn-sm">ADICIONAR</a></th>
                    </tr>
                </thead>
                <tbody>
                    <? 

                    foreach($result['resultSet'] as $dados){
                        
                    ?>
                    <tr data-id="<?=$dados['id']?>">
                    <td><?=$dados['from_condominio']?></td>
                    <td><?=$dados['from_bloco']?></td>
                    <td><?=$dados['from_unidade']?></td>
                    <td><?=$dados['nome']?></td>
                    <td><?=$dados['cpf']?></td>
                    <td><?=$dados['email']?></td>
                    <td><?=$dados['telefone']?></td>
                    <td><?=dateFormat($dados['dataCadastro'])?></td>
                    <td>
                        <a style="padding-right: 25px;" href="#" data-id="<?=$dados['id']?>" class="removerCliente"><i class="icofont-ui-delete botao"></i></a>
                        <a href="<?=$url_site?>cadastro/id/<?=$dados['id']?>"><i class="icofont-edit botao"></i></a>
                    </td>
                    </tr>
                    <?}?>
                    <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2" align="right" class="totalRegistros">Total Registros: <?=$totalRegistros?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <?=$paginacao?>
</div>