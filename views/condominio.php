<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Condomínio</h1>
</center>
    <form action="" method="POST" class="formCondominio">
        <div class="row">
            <div class="col-12 col-md-6">
                <input class="form-control mb-3" type="text" name="qtBloco" required placeholder="Quantidade de Blocos" value="<?=$popular['qtBloco']?>">
            </div>
            <div class="col-12 col-md-6">
                <input class="form-control mb-3" type="text" name="nomeCondo" required placeholder="Nome do Condomínio" value="<?=$popular['nomeCondo']?>">
            </div>
            <div class="col-12 col-md-4">
                <input class="form-control mb-3" type="text" name="logradouro" required placeholder="Logradouro" value="<?=$popular['logradouro']?>">
            </div>
            <div class="col-12 col-md-4">
                <input class="form-control mb-3" type="text" name="numero" required placeholder="Número" value="<?=$popular['numero']?>">
            </div>
            <div class="col-12 col-md-4">
                <input class="form-control mb-3" type="text" name="bairro" placeholder="Bairro" value="<?=$popular['bairro']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-3" type="text" name="cidade" required placeholder="Cidade" value="<?=$popular['cidade']?>">
            </div>
            <div class="col-12 col-md-3">
                <select class="form-control mb-3" name="estado" required value="<?=$popular['estado']?>">
                    <?foreach($estados as $key => $estado){
                        echo '<option value="'.$key.'">'.$estado.'</option>';
                    }?>
                </select>
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-3" type="text" name="cep" required placeholder="CEP" value="<?=$popular['cep']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control" type="text" name="from_sindico" required placeholder="Síndico" value="<?=$popular['from_sindico']?>">
            </div>
            <input type="hidden" name="from_adm" value="1">
            <?if($_GET['id']){?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <?}?>
            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>