<center class="mt-4 pb-4">
    <h1 class="col-12">Sistema de Condomínio</h1>
</center>
<div class="row">
    <div class="col-12 col-md-4 mt-3 mb-5">
        <div class="col-12 bg-info" style="border-radius:2em">
            <center class="paineis">
                <?
                $adm = new Admin();
                $result = $adm->getAdmin();
                ?>
                <span style="font-size: 50px;"><?=$result['totalResults']?></span><br>
                <span style="font-size: 35px;">Administradoras</span><br>
            </center>
        </div>
    </div>

    <div class="col-12 col-md-4 mt-3 mb-5">
        <div class="col-12 bg-info" style="border-radius:2em">
            <center class="paineis">
                <?
                $condominio = new Condominio();
                $result = $condominio->getCondominio();
                ?>
                <span style="font-size: 50px;"><?=$result['totalResults']?></span><br>
                <span style="font-size: 35px;">Condomínios</span><br>
            </center>
        </div>
    </div>

    <div class="col-12 col-md-4 mt-3 mb-5">
        <div class="col-12 bg-info" style="border-radius:2em">
            <center class="paineis">
                <?
                $morador = new Cadastro();
                $result = $morador->getClientes();
                ?>
                <span style="font-size: 50px;"><?=$result['totalResults']?></span><br>
                <span style="font-size: 35px;">Moradores</span><br>
            </center>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-6">
    <h2 align="center">Moradores por Condomínio</h2>
    <table class="table" border="1px">
        <thead align="center" class="thead-dark">
            <th scope="col">Condomínio</th>
            <th scope="col">Moradores</th>
        </thead>
        <?
        $morador = new Cadastro();
        $relacao = $morador->moradorCondo();
        // legivel($relacao);
        foreach($relacao['resultSet'] as $value){
        ?>
            <tr>
                <td align="center"><?=$value['nomeCondo']?></td>
                <td align="center"><?=($value['totalMoradores']) == 1 ? $value['totalMoradores'].' morador' : $value['totalMoradores'].' moradores'?></td>
            </tr>
        <?}?>
    </table>
    </div>
    <div class="col-12 col-md-6">
        <h2 align="center">Últimas Administradoras</h2>
        <table class="table" border="1px">
            <thead align="center" class="thead-dark">
                <th scope="col">Nome</th>
            </thead>
            <?
            $adm = new Admin();
            $relacao = $adm->ultimas();
            // legivel($relacao);
            foreach($relacao['resultSet'] as $value){
            ?>
                <tr>
                    <td align="center"><?=$value['nome_adm']?></td>
                </tr>
            <?}?>
        </table>
    </div>
</div>

<!-- <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
        <a href="index.php?page=listAdmin">
        <img src="https://www.undb.edu.br/hubfs/administra%C3%A7%C3%A3o%20undb-1.jpg" class="d-block w-100" style="width: 500px; height:700px; border-radius: 5em;" alt="...">
        <div class="carousel-caption d-none d-md-block">
            <h1 style="color: black;"><b>Administradoras</b></h1>
        </div>
        </a>
    </div>
    <div class="carousel-item">
        <a href="index.php?page=listCondominio">
        <img src="https://blog.laredo.com.br/wp-content/uploads/2019/03/276899-registro-de-imovel-novo-como-funciona-em-um-condominio-fechado-1024x767.jpg" style="width: 500px; height:700px; border-radius: 5em;" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
            <h1><b>Condomínios</b></h1>
        </div>
        </a>
    </div>
    <div class="carousel-item">
        <a href="index.php?page=clientes">
        <img src="https://cdn.sindiconet.com.br/Conteudos/607/associacao_956_667.jpg" style="width: 500px; height:700px; border-radius: 5em;" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <h1 style="color: black;"><b>Moradores</b></h1>
        </div>
        </a>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </button>
</div> -->
