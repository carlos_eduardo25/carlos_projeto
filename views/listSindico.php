<center class="mt-4 pb-4">
    <h1 class="col-12">Lista de Conselhos</h1>
</center>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table mt-3" id="listaSindicos">
                <thead class="thead-dark">
                    <tr>
                        <td colspan="10">
                            <form class="form-inline my-2 my-lg-0" action="index.php" method="GET" id="filtro">
                                <input type="hidden" name="page" value="listSindico">
                                <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome..." aria-label="Search" name="buscar">
                                <button class="btn btn-outline-success my-2 my-sm-0 mr-2" disabled type="submit">Buscar</button>
                                <a class="btn btn-outline-danger my-2 my-sm-0" href="<?=$url_site?>listSindico">Limpar</a>
                            </form>
                        </td>
                    </tr>
                    <tr>
                    <th scope="col">Síndico</th>
                    <th scope="col">Sub Síndico</th>
                    <th scope="col">Conselheiro</th>
                    <th scope="col">Condomínio</th>
                    <th scope="col">DT. Cadastro</th>
                    <th align="center"><a href="index.php?page=sindico" class="btn btn-info btn-sm">ADICIONAR</a></th>
                    </tr>
                </thead>
                <tbody>
                    <? 

                    foreach($result['resultSet'] as $dados){
                        
                    ?>
                    <tr data-id="<?=$dados['id']?>">
                    <td><?=$dados['sindico']?></td>
                    <td><?=$dados['subSindico']?></td>
                    <td><?=$dados['conselheiro']?></td>
                    <td><?=$dados['condoSindico']?></td>
                    <td><?=dateFormat($dados['dataCadastro'])?></td>
                    <td>
                        <a style="padding-right: 25px;" href="#" data-id="<?=$dados['id']?>" class="removerSindico"><i class="icofont-ui-delete botao"></i></a>
                        <a href="<?=$url_site?>sindico/id/<?=$dados['id']?>"><i class="icofont-edit botao"></i></a>
                    </td>
                    </tr>
                    <?}?>
                    <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="right" class="totalRegistros">Total Registros: <?=$totalRegistros?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <?=$paginacao?>
</div>