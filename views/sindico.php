<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Conselho</h1>
</center>
    <form action="" method="POST" class="formSindico">
        <div class="row">
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="sindico" required placeholder="Síndico" value="<?=$popular['sindico']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="subSindico" required placeholder="Sub Síndico" value="<?=$popular['subSindico']?>">
            </div>
            <div class="col-12 col-md-3">
                <input class="form-control mb-2" type="text" name="conselheiro" required placeholder="Conselheiro" value="<?=$popular['conselheiro']?>">
            </div>
            <div class="col-12 col-md-3">
                <select class="form-control" name="condoSindico">
                    <option value="N/A">Selecione o condomínio</option>
                    <?foreach($chamaCondo as $condo){?>
                        <option value="<?=$condo['id']?>" <?=($condo['id'] == $popular['condoSindico'] ? 'selected' : '')?>><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <?if($_GET['id']){?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <?}?>
            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>