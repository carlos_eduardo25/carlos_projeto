<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Unidade</h1>
</center>
    <form action="" method="POST" class="formUnidade">
        <div class="row">
            <div class="col-12 col-md-6">
                <select id="condominioUnidade" class="form-control mb-2 fromCondominio" name="condoUni">
                    <option value="N/A">Selecione o condomínio...</option>
                    <?foreach($chamaCondo as $condo){?>
                        <option value="<?=$condo['id']?>" <?=($condo['id'] == $popular['condoUni'] ? 'selected' : '')?>><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-6">
                <select id="blocoUnidade" class="form-control mb-2 fromBloco" name="blocoUni">
                    <?
                    if($_GET['id']){
                        $blocos = $unidades->getBlocoFromCond($popular['condoUni']);
                        foreach($blocos['resultSet'] as $bloco){
                    ?>
                            <option value="<?=$bloco['id']?>" <?=($bloco['id'] == $popular['blocoUni'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                        <?}?>
                    <?}?>

                </select>
            </div>
            <div class="col-12 col-md-4">
                <input class="form-control mb-2" type="text" name="numUnidade" required placeholder="N° da Unidade" value="<?=$popular['numUnidade']?>">
            </div>
            <div class="col-12 col-md-4">
                <input class="form-control mb-2" type="text" name="metragem" required placeholder="Metragem da Unidade" value="<?=$popular['metragem']?>">
            </div>
            <div class="col-12 col-md-4">
                <input class="form-control" type="text" name="garagem" required placeholder="Quantidade de Vagas de Garagem" value="<?=$popular['garagem']?>">
            </div>
            <?if($_GET['id']){?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <?}?>
            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>